"use strict";

var gulp = require('gulp');
var notify = require("gulp-notify");
var rename = require("gulp-rename");
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;
var wiredep = require('wiredep').stream; 
var jade = require('gulp-jade');
var prettify = require('gulp-html-prettify');
var buffer = require('vinyl-buffer');
var merge = require('merge-stream');
var critical = require('critical');
var del = require('del');
var zip = require('gulp-zip');


// Styles
var sass = require('gulp-sass');
var csscomb = require('gulp-csscomb');
var gcmq = require('gulp-group-css-media-queries');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

// Icons
var realFavicon = require('gulp-real-favicon');
var fs = require('fs');


// Scripts
var uglify = require('gulp-uglify');


// Build
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var gulpFilter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');


// Image
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');

//  Svg
var svgSprite = require("gulp-svg-sprites");
var svgmin = require('gulp-svgmin');
var cheerio = require('gulp-cheerio');
var replace = require('gulp-replace');



// ------ SVG Sprite ------
 
gulp.task('sprites', function () {
    return gulp.src('images/svg/*.svg')
        .pipe(svgSprite({
            padding: 5,
            templates: { scss: true }
        }))
        .pipe(gulp.dest('images/svg-sprite/'));
});

gulp.task('svgSpriteBuild', function () {
    return gulp.src('images/svg/*.svg')
        // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill and style declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        // cheerio plugin create unnecessary string '>', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
                mode: "symbols",
                preview: false,
                selector: "icon-%f",
                svg: {
                    symbols: 'symbol_sprite.html'
                }
            }
        ))
        .pipe(gulp.dest('images/svg-sprite'));
});

// create sass file for our sprite
gulp.task('svgSpriteSass', function () {
    return gulp.src('images/svg/*.svg')
        .pipe(svgSprite({
                preview: false,
                selector: "icon-%f",
                svg: {
                    sprite: 'svg_sprite.html'
                },
                cssFile: '../../sass/components/_svg-sprite.scss',
                templates: {
                    css: require("fs").readFileSync('sass/mixins/_sprite-template.scss', "utf-8")
                }
            }
        ))
        .pipe(gulp.dest('images/svg-sprite/'));
});

gulp.task('svgSprite', ['svgSpriteBuild', 'svgSpriteSass']);

//------ HTML prettify Task ------

gulp.task('prettyhtml', function() {
  gulp.src('*.html')
    .pipe(prettify({indent_char: ' ', indent_size: 4}))
    .pipe(gulp.dest('./'))
});

gulp.task('prettyhtml-export', function() {
  gulp.src('dist/*.html')
    .pipe(prettify({indent_char: ' ', indent_size: 4}))
    .pipe(gulp.dest('./'))
});

gulp.task('critical-init', ['export'], function(cb) {
    gulp.src('dist/index.html')
        .pipe(rename('index-original.html'))
        .pipe(gulp.dest('dist/'))
        setTimeout(function () {
            cb();
        }, 1000);
});

gulp.task('critical', ['critical-init'], function(cb) {
    critical.generate({
        inline: true,
        base: 'dist/',
        src: 'index-original.html',
        dest: 'index.html',
        minify: true,
        width: 1300,
        height: 900
    });
});

// ------ Livereload Tasks ------
 
gulp.task('jade', function() {
  gulp.src('jade/pages/*.jade')
    .pipe(jade({
        pretty: true
        })
    )
    .pipe(gulp.dest('./'))
    .pipe(browserSync.stream());
});

gulp.task('jade-html', function() {
  gulp.src('jade/**/*.jade')
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest('./html'))
});

gulp.task('jade:watch', function() {
    gulp.watch('jade/**/*.jade', ['jade']);
});

gulp.task('sass', function() {
    return gulp.src('sass/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer([
            'Android 2.3',
            'Android >= 4',
            'Chrome >= 15',
            'Firefox >= 15', // Firefox 24 is the latest ESR 
            'Explorer >= 8',
            'iOS >= 6',
            'Opera >= 12',
            'Safari >= 5'
        ]))
        .pipe(gcmq())
        .pipe(rename('main.css'))
        .pipe(gulp.dest('css/'))
        .pipe(minifyCSS(''))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest('css/'))
        .pipe(browserSync.stream());
});

gulp.task('sass:watch', function() {
    gulp.watch('sass/**/*.scss', ['sass']);
});


gulp.task('browser-sync', function() {
    browserSync.init({
       server: "../../",
       startPath: 'project/leaderboard',
       open: "external"
    });
});

gulp.task('livereload', ['browser-sync', 'jade:watch','sass:watch']);


// ------ Bower install Dependencies ------

gulp.task('bower', function() {
    gulp.src('jade/layouts/_head.jade')
        .pipe(wiredep({
            directory: "vendor",
            ignorePath: /(\.\.\/\.\.\/)/,
            bowerJson: require('./bower.json'),
        }))
        .pipe(notify("Complete!"))
        .pipe(gulp.dest('jade/layouts/'));
});

gulp.task('bower:watch', function() {
    gulp.watch('bower.json', ['bower']);
});

// ------ Images Sprite ------

gulp.task('sprite', function() {
    var spriteData = gulp.src('images/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../images/sprite.png',
        cssName: '_sprite.scss',
        padding: 5,
        algorithm: 'binary-tree'
    }));

    var imgStream = spriteData.img
        // DEV: We must buffer our stream into a Buffer for `imagemin` 
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest('images/'));

    // Pipe CSS stream through CSS optimizer and onto disk 
    var cssStream = spriteData.css

        .pipe(gulp.dest('sass/mixins/'));

    // Return a merged stream to handle both `end` events 
    return merge(imgStream, cssStream);
});

// --------- Export Project -----------------

gulp.task('useref',['export-folders'], function() {
    return gulp.src('*.html')
        .pipe(useref())
        .pipe(gulpif('*.css', minifyCSS()))
        .pipe(gulp.dest('dist/'));
});

gulp.task('bowermainfiles', function() {
    var filter = gulpFilter('**/*.*', { restore: true });
    return gulp.src(mainBowerFiles())
        .pipe(filter)
        .pipe(gulpif('*.{eot,svg,ttf,woff,woff2}', gulp.dest('dist/fonts/')))
        .pipe(gulpif('*.css', gulp.dest('dist/css/vendor/')))
        .pipe(gulpif('*.js', gulp.dest('dist/js/vendor/')))
});

gulp.task('export-folders', function() {
    return gulp.src(['.htaccess', '!bower.json', '!faviconData.json', '!./sass{,/**}', '!gulpfile.js', './**', '!./jade{,/**}', '!./vendor{,/**}'])
        .pipe(gulpif('pics/**/*.{png,jpg,jpeg}', imagemin()))
        .pipe(gulpif('images/**/*.{png,jpg,jpeg}', imagemin()))
        .pipe(gulp.dest('dist/'));
});

gulp.task('export', ['useref','bowermainfiles']);


gulp.task('zip', () =>
    gulp.src(['dist/**/*.*', 'dist/.htaccess', '!dist/.git/**/*.*'])
        .pipe(zip('template.zip'))
        .pipe(notify("Archieved!"))
        .pipe(gulp.dest('./'))
);

gulp.task('clean', function(cb) {
    del(['dist'], cb);
});

// ------ Generate and inject Favicons ------

// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
    realFavicon.generateFavicon({
        masterPicture: 'images/master_picture.png',
        dest: 'favicons/',
        iconsPath: 'favicons',
        design: {
            ios: {
                pictureAspect: 'backgroundAndMargin',
                backgroundColor: '#ffffff',
                margin: '14%',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: true,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                }
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'noChange',
                backgroundColor: '#2d89ef',
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: true,
                    windows10Ie11EdgeTiles: {
                        small: true,
                        medium: true,
                        big: true,
                        rectangle: true
                    }
                }
            },
            androidChrome: {
                pictureAspect: 'shadow',
                themeColor: '#ffffff',
                manifest: {
                    display: 'browser',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'silhouette',
                themeColor: '#5bbad5'
            }
        },
        settings: {
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false
        },
        markupFile: FAVICON_DATA_FILE
    }, function() {
        done();
    });
});

// Inject the favicon markups in your HTML pages. You should run
// this task whenever you modify a page. You can keep this task
// as is or refactor your existing HTML pipeline.
gulp.task('inject-favicon', function() {
    return gulp.src(['jade/layouts/_head.jade'])
        .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
        .pipe(gulp.dest('jade/layouts/'));
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
    var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
    realFavicon.checkForUpdates(currentVersion, function(err) {
        if (err) {
            throw err;
        }
    });
});

// ---------------------------------