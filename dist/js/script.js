var slider_timeout = 10; /* seconds */
var winner_block_timeout = 3000; /* miliseconds */
var timeout_notification = 60; /* seconds */
var report_type = '';
var main_link = 'json/report.json';
var reports_link = {
    'buying': {
        'phase': 'get_reports',
        'type': 'buying'
    },
    'sales': {
        'phase': 'get_reports',
        'type': 'sales'
    }
};

$( document ).ready(function() {
    $('body').on('click','.leaderboard-navigation li',function(){
        if(!$(this).hasClass('active')){
          slideIndex = parseInt($(this).data('rating'));
          report.reports.init(slideIndex);
        }
    });

    timer.init();
    notifications.get();
    report.reports.get(report_type);
});

var report = {
    _slideIndex: 0,
    _stop: false,
    _sliderTimeout : 0,
    _reportsArr: [],
    _lastSlide: 0,
    stop: function(){
        clearTimeout(report._sliderTimeout);
        report._stop = true;
    },
    start: function(){
        report._stop = false;
        report.show();
    },
    show: function(next = false){
        report._slideIndex++;
        let all_empty = 0;
        if (report._slideIndex > report._reportsArr.length) {
            report._slideIndex = 1;
            if(report._lastSlide === 0){ /*if all reports are empty then stop for 5 min*/
                all_empty = 1;
            }
        }
        if( all_empty === 0 ) {
            report.reports.init(report._reportsArr[report._slideIndex - 1].id);
        }

        if (!stop) {
            let slide_time = slider_timeout;
            if( next && (report._lastSlide === report._slideIndex || all_empty) ) {
                slide_time = 300;
            }
            report._sliderTimeout = setTimeout(report.show, slider_timeout * 1000);
        }
    },
    getNextSlide: function(){
        clearTimeout(report._sliderTimeout);
        report.show(true);
    },
    reports : {
        get: function(type){
            $.ajax({
                type : 'GET',
                dataType : 'json',
                url: main_link,
                success : function(data) {
                    if(true){
                        $('.leaderboard-navigation').html('');
                        $('#leaderboardNavigationTemplate').tmpl(data).appendTo(".leaderboard-navigation");
                        report._reportsArr = data;
                        report.start();
                    } else {
                        Swal.fire({
                            title: 'Oops...',
                            text: data.error_message,
                            type: 'error'
                        })
                    }
                }
            });
        },
        init: function(id){
            var reportInfo = report.reports.getReport(id);

            report.users.get(reportInfo.url);

            $('.leaderboard-navigation li.active').removeClass('active');
            $('.leaderboard-navigation li[data-rating=' + reportInfo.id + ']').addClass('active');
            $('.leaderboard-name').attr('data-title', reportInfo.name);

            // report.reports.initBackground(reportInfo.images); /* Removed backgrounds, to start, change bg for .page-container for transparent*/
        },
        initBackground: function(images){
            var slides = [];

            for ( var key in images) {
                slides.push({src : images[key]});
            }

            if( slides.length ) {
                if ($('body').is('.vegas-container')) {
                    $('body').vegas('destroy');
                }

                $('body').vegas ({
                    overlay: true,
                    transition: 'blur',
                    transitionDuration: 1000,
                    delay: 10000,
                    timer: false,
                    slides: slides,
                    overlay: 'images/overlays/08.png'
                });
            }
        },
        getReport: function(id){
            return report._reportsArr.find(item => item.id === id);
        }
    },
    users : {
        get: function(url){
            $.ajax({
                type : 'GET',
                dataType : 'json',
                url: url,
                beforeSend: function () {
                    $('.leaderboard').html('');
                    report.winnerBlock.update('');
                },
                success : function(data) {
                    if(true) {
                        if( data.length ) {
                        // if( true ) {
                            $('#leaderboardItemTemplate').tmpl(data).appendTo(".leaderboard");

                            if ($('.leaderboard').data('isotope')) {
                                $('.leaderboard').isotope('destroy');
                            }
                            report._lastSlide = report.slideIndex;
                            report.users.sort(data);
                        } else {
                            report.getNextSlide()
                        }
                    }
                }
            });
        },
        sort: function (data){
            report.users._isotopeSort();

            var places = ['', 'gold', 'silver', 'bronze'];
            for (i = 1; i <= Math.min(data.length, places.length); i++){
                var info = report.users.getPlace(data, i);
                $('div[data-score="' + info.score + '"]').addClass(places[i]);
            }

            var first_place = report.users.getPlace(data, 1);
            var emptyUserImage = 'images/empty_user.png';

            if(typeof first_place !== "undefined"){
                let userImages = [];
                for (var i = 0; i < first_place.users.length; i++) {
                    if(first_place.users[i].avatar === ''){
                        userImages.push(emptyUserImage)
                    } else {
                        userImages.push(first_place.users[i].avatar)
                    }
                }
                report.winnerBlock.update(userImages);
            } else {
                report.winnerBlock.update(emptyUserImage);
                $('.particles-js-canvas-el').remove();
            }
        },
        getPlace: function(array, place){
            var user_info = array.sort(
                function(a, b) {
                    return parseFloat(b['score']) - parseFloat(a['score']);
                }
            )[place - 1]
            return user_info;
        },
        _isotopeSort: function(data){
            var $grid = $('.leaderboard').isotope({
                    itemSelector: '.leaderboard-item',
                    getSortData: {
                        number: '[sort-value] parseInt'
                    }
                });

            $grid.isotope('updateSortData');
            $grid.isotope({ sortBy: 'number',sortAscending: false });

            var filteredElements = 0;
            var gridElements = $grid.isotope('getItemElements');

            $grid.isotope( 'on', 'arrangeComplete', function( filteredItems ) {
                filteredElements = filteredItems.length;
                $.each(gridElements, function( index, value ) {
                    $(value).css('background-color', 'var(--sort-default-bg)');
                });
                for (i = 0; i < filteredItems.length; i++) {
                    if((i+1)%2==0){
                        if(filteredItems[i] !== undefined) { /* Set background for 2n position */
                            $(filteredItems[i].element).css({
                                'background-color': 'var(--sort-even-bg)'
                            });
                        }
                    }
                    if(filteredItems[0] !== undefined) { /* Set background for winner */
                        $(filteredItems[0].element).css({
                            'background-color': 'var(--sort-winner-bg)'
                        });
                    } else { break; }

                }
            });

            $grid.isotope( 'on', 'layoutComplete', function( laidOutItems ) {
                if(filteredElements == 0){
                    var gridElements = $grid.isotope('getItemElements');
                    $.each(gridElements, function( index, value ) {
                        $(value).css('background-color', 'var(--sort-default-bg)');
                    });
                    $.each(gridElements, function( index, value ) {
                        $(value).css('background-color', 'var(--sort-default-bg)');
                        if(parseInt(index + 1) % 2 == 0){
                            $(value).css('background-color', 'var(--sort-even-bg)');
                        }
                        if(index == 0){
                            $(value).css('background-color', 'var(--sort-winner-bg)');
                        }
                    });
                }
            });
        }
    },
    winnerBlock : {
        _winnerImageSlideIndex: 0,
        _winnerImageTimeoutVar: '',
        update: function(avatar){
            $('.user-info').html('');
            if(avatar != ''){
                let html = '';
                for (var i = 0; i < avatar.length; i++) {
                    html += '<img class="user-avatar z-depth-4 avatar-fade" src="' + avatar[i] + '">'
                }
                $('.user-info').html(html);

                report._winnerImageSlideIndex = 0;
                if(avatar.length > 1){
                    report.winnerBlock._carousel()
                } else {
                    clearTimeout(report._winnerImageTimeoutVar);
                }
                particles.init();
            }
        },
        _carousel: function(){
            var slides = document.getElementsByClassName("user-avatar");
            
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            report._winnerImageSlideIndex++;

            if (report._winnerImageSlideIndex > slides.length) {
                report._winnerImageSlideIndex = 1
                
            }

            slides[report._winnerImageSlideIndex-1].style.display = "block";

            clearTimeout(report._winnerImageTimeoutVar);
            report._winnerImageTimeoutVar = setTimeout(report.winnerBlock._carousel, winner_block_timeout); // Change image every 3 seconds
        }
    }
}

var timer = {
    _settings: function(){
        var today = new Date();
        var day = 16;
        var month = today.getMonth() + 1;

        var timer_date = `${today.getFullYear()}/${month}/${day}`;
        return timer_date
    },
    init: function(){
        $('#month-countdown').countdown(timer._settings(), function (event) {
            $(this).html(event.strftime(''
            + '<div><span>%D</span><span>days</span></div> '
            + '<div><span>%H</span><span>hr</span></div> '
            + '<div><span>%M</span><span>min</span></div> '
            + '<div><span>%S</span><span>sec</span></div>'));
        });
  }
}

var particles = {
    init: function(){
        $('.particles-js-canvas-el').remove();
        particlesJS("particles-js", {
            "particles": {
              "number": {
                "value": 250,
                "density": {
                  "enable": true,
                  "value_area": 157.82952832645452
                }
              },
              "color": {
                "value": "#fff"
              },
              "shape": {
                "type": "circle",
                "stroke": {
                  "width": 0,
                  "color": "#000000"
                },
                "polygon": {
                  "nb_sides": 4
                },
                "image": {
                  "src": "img/github.svg",
                  "width": 100,
                  "height": 100
                }
              },
              "opacity": {
                "value": 1,
                "random": true,
                "anim": {
                  "enable": true,
                  "speed": 5,
                  "opacity_min": 0,
                  "sync": false
                }
              },
              "size": {
                "value": 1,
                "random": true,
                "anim": {
                  "enable": false,
                  "speed": 4,
                  "size_min": 0.3,
                  "sync": false
                }
              },
              "line_linked": {
                "enable": false,
                "distance": 20,
                "color": "#ffffff",
                "opacity": 0.4,
                "width": 1
              },
              "move": {
                "enable": true,
                "speed": 1,
                "direction": "none",
                "random": true,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                  "enable": false,
                  "rotateX": 600,
                  "rotateY": 600
                }
              }
            },
            "interactivity": {
              "detect_on": "canvas",
              "events": {
                "onhover": {
                  "enable": false
                },
                "onclick": {
                  "enable": false
                },
                "resize": true
              },
              "modes": {
                "grab": {
                  "distance": 400,
                  "line_linked": {
                    "opacity": 1
                  }
                },
                "bubble": {
                  "distance": 250,
                  "size": 0,
                  "duration": 2,
                  "opacity": 0, 
                  "speed": 3
                },
                "repulse": {
                  "distance": 400,
                  "duration": 0.4
                },
                "push": {
                  "particles_nb": 4
                },
                "remove": {
                  "particles_nb": 2
                }
              }
            },
            "retina_detect": true
        });
    }
}

var notifications = {
    get: function(){
        $.ajax({
            type : 'GET',
            dataType : 'json',
            url: 'json/notification.json',
            success : function(data) {
                if(true) {
                    $('.notification li').fadeOut('fast', function () {
                        $(this).remove()
                    });
                    $('#notificationItemTemplate').tmpl(data).appendTo(".notification");
                } else {
                    Swal.fire({
                        title: 'Oops...',
                        text: data.error_message,
                        type: 'error'
                    })
                }
            }
        });

        setTimeout(function() {
            notifications.get('json/notification.json')
        }, timeout_notification * 1000);
    }
}
